<?php
/*
Plugin Name: Dog Breed Calculator
Description: Dog Breed Calculator
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

add_action('wp_enqueue_scripts', 'dog_breed_script_front_css');
add_action('wp_enqueue_scripts', 'dog_breed_script_front_js');
add_action('admin_enqueue_scripts', 'dog_breed_script_back_css');
add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);
function order_meta_handler($item_id, $values, $cart_item_key) {
	$cart_session = WC()->session->get('cart');
	if($cart_session[$cart_item_key]['country_name'][0]){
	wc_add_order_item_meta($item_id, "Country Name", $cart_session[$cart_item_key]['country_name'][0]);
	}
	if($cart_session[$cart_item_key]['dog_weight'][0]){
	wc_add_order_item_meta($item_id, "Dog weight", $cart_session[$cart_item_key]['dog_weight'][0]);
	}
	if($cart_session[$cart_item_key]['dog_food_type'][0]){
	wc_add_order_item_meta($item_id, "Dog food type", $cart_session[$cart_item_key]['dog_food_type'][0]);
	}
	if($cart_session[$cart_item_key]['neutered'][0]){
	wc_add_order_item_meta($item_id, "Neutered", $cart_session[$cart_item_key]['neutered'][0]);
	}
	if($cart_session[$cart_item_key]['dogbreeds'][0]){
	wc_add_order_item_meta($item_id, "Dog Breed", $cart_session[$cart_item_key]['dogbreeds'][0]);
	}
	if($cart_session[$cart_item_key]['gender'][0]){
	wc_add_order_item_meta($item_id, "Gender", $cart_session[$cart_item_key]['gender'][0]);
	}
	if($cart_session[$cart_item_key]['age'][0]){
	wc_add_order_item_meta($item_id, "Dog Age", $cart_session[$cart_item_key]['age'][0]);
	}
	if($cart_session[$cart_item_key]['how_active'][0]){
	wc_add_order_item_meta($item_id, "Active", $cart_session[$cart_item_key]['how_active'][0]);
	}
	if($cart_session[$cart_item_key]['dog_type'][0]){
	wc_add_order_item_meta($item_id, "Dog Type", $cart_session[$cart_item_key]['dog_type'][0]);
	}
	if($cart_session[$cart_item_key]['country_name'][0]){
	wc_add_order_item_meta($item_id, "Country Name", $cart_session[$cart_item_key]['country_name'][0]);
	}
	
}
add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data11', 10, 2);
function add_cart_item_custom_data11($cart_item_meta, $new_post_id) {
	$dog_weight = get_post_meta( $new_post_id,'dog_weight'  );
	$how_active= get_post_meta( $new_post_id,'how_active');
	$dog_type= get_post_meta( $new_post_id,'dog_type');
	$user_name= get_post_meta( $new_post_id,'user_name');
	$user_email= get_post_meta( $new_post_id,'user_email');
	$dog_food_type= get_post_meta( $new_post_id,'dog_food_type');
	$neutered= get_post_meta( $new_post_id,'neutered');
	$dogbreeds= get_post_meta( $new_post_id,'dogbreeds');
	$gender= get_post_meta( $new_post_id,'gender');
	$age= get_post_meta( $new_post_id,'age');
	$country_name = get_post_meta( $new_post_id,'country_name' );
	$cart_item_meta['dog_weight'] = $dog_weight;
	$cart_item_meta['how_active'] = $how_active;
	$cart_item_meta['neutered'] = $neutered;
	$cart_item_meta['dog_type'] = $dog_type;
	$cart_item_meta['user_name'] = $user_name;
	$cart_item_meta['user_email'] = $user_email;
	$cart_item_meta['dogbreeds'] = $dogbreeds;
	$cart_item_meta['age'] = $age;
	$cart_item_meta['country_name'] = $country_name;
	return $cart_item_meta;
}
/**
 * Register a custom menu page.
 */
function wpdocs_register_my_custom_menu_page(){
    add_menu_page( 
        __( 'Dog Breed Setting', 'textdomain' ),
        'Dog Breed',
        'manage_options',
        'dog_breed',
        'dog_breed_menu_page'
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
 
/**
 * Display a custom menu page
 */
function dog_breed_menu_page(){
	if(isset($_POST['submit_feed_setting'])){
		$kcal_per_food = $_POST['kcal_per_food']; 
		$price_per_kg = $_POST['price_per_kg']; 
		$threepl = $_POST['3PL'];
		$packaging  = $_POST['packaging'];
		$cart_page_disc  = $_POST['cart_page_disc'];

		update_option('kcal_per_food',$kcal_per_food);
		update_option('price_per_kg',$price_per_kg);
		update_option('threepl',$threepl);
		update_option('packaging',$packaging);
		update_option('cart_page_disc',$cart_page_disc);
	}
	$kcal_per_food = get_option('kcal_per_food');
	$price_per_kg = get_option('price_per_kg');
	$threepl = get_option('threepl');
	$packaging = get_option('packaging');
	$cart_page_disc_show = get_option('cart_page_disc');

   ?>
   <form action="" class="ci_form" id="form" method="post">
<h2>Dog Breed Setting</h2>

<div class="">
<label>Kcal per dog food kilogram</label>

<input type="text" class="form-control dog_name_1" placeholder="Kcal per dog food kilogram" value="<?php echo $kcal_per_food ?>" required="required" name="kcal_per_food" >
</div><br>

<div class="">
<label>Dog food price per kilo</label>
<input type="text" class="form-control dog_name_1" placeholder="Dog food price per kilo" value="<?php echo $price_per_kg ?>" required="required" name="price_per_kg">
</div><br>
<div class="">
<label>3PL</label>
<input type="text" class="form-control dog_name_1" placeholder="3PL" required="required" value="<?php echo $threepl ?>" name="3PL">
</div><br>
<div class="">
<label>PACKAGING</label>
<input type="text" class="form-control dog_name_1" placeholder="PACKAGING" required="required" value="<?php echo $packaging ?>" name="packaging">
</div><br>
<div class="">
<label>Card page Discount in Percentage(Enter value from 0-100)</label><br/>
<input type="text" class="form-control cart_page_disc" placeholder="Enter value from 0-100"  value="<?php echo $cart_page_disc_show ?>" name="cart_page_disc">
</div><br>
<input type="submit" name="submit_feed_setting" id="feed_setting" value="Submit">
</form>
   <?php
}
add_action('init', 'wp_dog_breed_init');
function wp_dog_breed_init() {
   add_shortcode('show_dog_breed_form', 'wp_dog_breed_form');
}
function get_zone_by_country($country){

if($country == 'Austria (AT)' || $country == 'Belgium (BE)' || $country == 'Bulgaria (BG)' || $country == 'Croatia (HR)' || $country == 'Cyprus (CY)' || $country == 'Czech Rep., The (CZ)' || $country == 'Denmark (DK)' || $country == 'Finland (FI)' || $country == 'France (FR)' || $country == 'Germany (DE)' || $country == 'Greece (GR)' || $country == 'Hungary (HU)' || $country == 'Ireland, Rep. Of (IE)' || $country == 'Italy (IT)' || $country == 'Luxembourg (LU)' || $country == 'Malta (MT)' || $country == 'Netherlands, The (NL)' || $country == 'Portugal (PT)' || $country == 'Romania (RO)' || $country == 'Slovakia (SK)' || $country == 'Slovenia (SI)' || $country == 'Spain (ES)' || $country == 'Sweden (SE)' || $country == 'United Kingdom (GB)'){
	$zone = 2;
	return $zone;
}elseif($country == 'Estonia (EE)' || $country == 'Latvia (LV)' || $country == 'Poland (PL)'){
	$zone = 1;
	return $zone;
}elseif($country == 'Switzerland (CH)'){
	$zone = 3;
	return $zone;
}elseif($country == 'Canada (CA)' || $country == 'USA'){
	$zone = 4;
	return $zone;
}elseif($country == 'Australia (AU)'){
	$zone = 5;
	return $zone;
}
}

function get_shipping_rate_by_country_weight($country,$weight){
$zone = get_zone_by_country($country);
	if($zone == 2){
		$weight_rate_arr_zone2 = array(
'0.5' => '15.79' ,
		'1.0' => '15.79' ,
		'1.5' => '15.79' ,
		'2.0' => '15.79' ,
		'2.5' => '23.40' ,
		'3.0' => '23.40' ,
		'3.5' => '23.40' ,
		'4.0' => '23.40' ,
		'4.5' => '23.40' ,
		'5.0' => '23.40' ,
		'5.5' => '29.84' ,
		'6.0' => '31.26' ,
		'6.5' => '32.67' ,
		'7.0' => '34.09' ,
		'7.5' => '41.98' ,
		'8.0' => '43.64' ,
		'8.5' => '45.31' ,
		'9.0' => '46.98' ,
		'9.5' => '48.65' ,
		'10.0' => '50.31' ,
		'10.5' => '51.88' ,
		'11.0' => '53.44' ,
		'11.5' => '55.00' ,
		'12.0' => '56.57' ,
		'12.5' => '58.13' ,
		'13.0' => '59.70' ,
		'13.5' => '61.26' ,
		'14.0' => '62.82' ,
		'14.5' => '64.39' ,
		'15.0' => '65.95' ,
		'15.5' => '67.52' ,
		'16.0' => '69.08' ,
		'16.5' => '70.64' ,
		'17.0' => '72.21' ,
		'17.5' => '73.77' ,
		'18.0' => '75.34' ,
		'18.5' => '76.90' ,
		'19.0' => '78.46' ,
		'19.5' => '80.03' ,
		'20.0' => '81.59' ,
		'20.5' => '83.16' ,
		'21.0' => '84.72' ,
		'21.5' => '86.28' ,
		'22.0' => '87.85' ,
		'22.5' => '89.41' ,
		'23.0' => '90.98' ,
		'23.5' => '92.54' ,
		'24.0' => '94.10' ,
		'24.5' => '95.67' ,
		'25.0' => '97.23' ,
		'25.5' => '98.80' ,
		'26.0' => '100.36' ,
		'26.5' => '101.92' ,
		'27.0' => '103.49' ,
		'27.5' => '105.05' ,
		'28.0' => '106.62' ,
		'28.5' => '108.18' ,
		'29.0' => '109.74' ,
		'29.5' => '111.31' ,
		'30.0' => '112.87' ,
		'31.0' => '115.84' ,
		'32.0' => '118.81' ,
		'33.0' => '121.77' ,
		'34.0' => '124.74' ,
		'35.0' => '127.71' ,
		'36.0' => '130.67' ,
		'37.0' => '133.64' ,
		'38.0' => '136.61' ,
		'39.0' => '139.58' ,
		'40.0' => '142.54' ,
		'41.0' => '145.51' ,
		'42.0' => '148.48' ,
		'43.0' => '151.44' ,
		'44.0' => '154.41' ,
		'45.0' => '157.38' ,
		'46.0' => '160.34' ,
		'47.0' => '163.31' ,
		'48.0' => '166.28' ,
		'49.0' => '169.25' ,
		'50.0' => '172.21' ,
		'51.0' => '175.18' ,
		'52.0' => '178.15' ,
		'53.0' => '181.11' ,
		'54.0' => '184.08' ,
		'55.0' => '187.05' ,
		'56.0' => '190.01' ,
		'57.0' => '192.98' ,
		'58.0' => '195.95' ,
		'59.0' => '198.92' ,
		'60.0' => '201.88' ,
		'61.0' => '204.85' ,
		'62.0' => '207.82' ,
		'63.0' => '210.78' ,
		'64.0' => '213.75' ,
		'65.0' => '216.72' ,
		'66.0' => '219.68' ,
		'67.0' => '222.65' ,
		'68.0' => '225.62' ,
		'69.0' => '228.59' ,
		'70.0' => '231.55');
		foreach ($weight_rate_arr_zone2 as $weight_s => $rate_s){
			if($weight_s == $weight){
				return $rate_s;
			}
			
		}
	}elseif($zone == 1){
		$weight_rate_arr_zone1 = array(
		'0.5' => '12.41' ,
		'1.0' => '12.41' ,
		'1.5' => '12.41' ,
		'2.0' => '12.41' ,
		'2.5' => '18.73' ,
		'3.0' => '18.73' ,
		'3.5' => '18.73' ,
		'4.0' => '18.73' ,
		'4.5' => '18.73' ,
		'5.0' => '18.73' ,
		'5.5' => '24.53' ,
		'6.0' => '25.81' ,
		'6.5' => '27.08' ,
		'7.0' => '28.36' ,
		'7.5' => '35.01' ,
		'8.0' => '36.51' ,
		'8.5' => '38.02' ,
		'9.0' => '39.53' ,
		'9.5' => '41.03' ,
		'10.0' => '42.54' ,
		'10.5' => '43.93' ,
		'11.0' => '45.32' ,
		'11.5' => '46.71' ,
		'12.0' => '48.10' ,
		'12.5' => '49.50' ,
		'13.0' => '50.89' ,
		'13.5' => '52.28' ,
		'14.0' => '53.67' ,
		'14.5' => '55.06' ,
		'15.0' => '56.45' ,
		'15.5' => '57.85' ,
		'16.0' => '59.24' ,
		'16.5' => '60.63' ,
		'17.0' => '62.02' ,
		'17.5' => '63.41' ,
		'18.0' => '64.80' ,
		'18.5' => '66.19' ,
		'19.0' => '67.59' ,
		'19.5' => '68.98' ,
		'20.0' => '70.37' ,
		'20.5' => '71.76' ,
		'21.0' => '73.15' ,
		'21.5' => '74.54' ,
		'22.0' => '75.93' ,
		'22.5' => '77.33' ,
		'23.0' => '78.72' ,
		'23.5' => '80.11' ,
		'24.0' => '81.50' ,
		'24.5' => '82.89' ,
		'25.0' => '84.28' ,
		'25.5' => '85.68' ,
		'26.0' => '87.07' ,
		'26.5' => '88.46' ,
		'27.0' => '89.85' ,
		'27.5' => '91.24' ,
		'28.0' => '92.63' ,
		'28.5' => '94.02' ,
		'29.0' => '95.42' ,
		'29.5' => '96.81' ,
		'30.0' => '98.20' ,
		'31.0' => '100.76' ,
		'32.0' => '103.33' ,
		'33.0' => '105.89' ,
		'34.0' => '108.46' ,
		'35.0' => '111.02' ,
		'36.0' => '113.59' ,
		'37.0' => '116.15' ,
		'38.0' => '118.71' ,
		'39.0' => '121.28' ,
		'40.0' => '123.84' ,
		'41.0' => '126.41' ,
		'42.0' => '128.97' ,
		'43.0' => '131.54' ,
		'44.0' => '134.10' ,
		'45.0' => '136.67' ,
		'46.0' => '139.23' ,
		'47.0' => '141.80' ,
		'48.0' => '144.36' ,
		'49.0' => '146.92' ,
		'50.0' => '149.49' ,
		'51.0' => '152.05' ,
		'52.0' => '154.62' ,
		'53.0' => '157.18' ,
		'54.0' => '159.75' ,
		'55.0' => '162.31' ,
		'56.0' => '164.88' ,
		'57.0' => '167.44' ,
		'58.0' => '170.00' ,
		'59.0' => '172.57' ,
		'60.0' => '175.13' ,
		'61.0' => '177.70' ,
		'62.0' => '180.26' ,
		'63.0' => '182.83' ,
		'64.0' => '185.39' ,
		'65.0' => '187.96' ,
		'66.0' => '190.52' ,
		'67.0' => '193.09' ,
		'68.0' => '195.65' ,
		'69.0' => '198.21' ,
		'70.0' => '200.78');
		foreach ($weight_rate_arr_zone1 as $weight_s => $rate_s){
				if($weight_s == $weight){
					return $rate_s;
				}
				
			}
		
	}elseif($zone == 3){
		$weight_rate_arr_zone3 = array(
		'0.5' => '20.46 ' ,
		'1.0' => '20.46 ' ,
		'1.5' => '20.46 ' ,
		'2.0' => '20.46 ' ,
		'2.5' => '28.09 ' ,
		'3.0' => '28.09 ' ,
		'3.5' => '28.09 ' ,
		'4.0' => '28.09 ' ,
		'4.5' => '28.09 ' ,
		'5.0' => '28.09 ' ,
		'5.5' => '34.74 ' ,
		'6.0' => '36.31 ' ,
		'6.5' => '37.87 ' ,
		'7.0' => '39.43 ' ,
		'7.5' => '48.43 ' ,
		'8.0' => '50.27 ' ,
		'8.5' => '52.11 ' ,
		'9.0' => '53.95 ' ,
		'9.5' => '55.79 ' ,
		'10.0' => '57.63 ' ,
		'10.5' => '59.27 ' ,
		'11.0' => '60.92 ' ,
		'11.5' => '62.56 ' ,
		'12.0' => '64.20 ' ,
		'12.5' => '65.85 ' ,
		'13.0' => '67.49 ' ,
		'13.5' => '69.14 ' ,
		'14.0' => '70.78 ' ,
		'14.5' => '72.43 ' ,
		'15.0' => '74.07 ' ,
		'15.5' => '75.72 ' ,
		'16.0' => '77.36 ' ,
		'16.5' => '79.01 ' ,
		'17.0' => '80.65 ' ,
		'17.5' => '82.29 ' ,
		'18.0' => '83.94 ' ,
		'18.5' => '85.58 ' ,
		'19.0' => '87.23 ' ,
		'19.5' => '88.87 ' ,
		'20.0' => '90.52 ' ,
		'20.5' => '92.16 ' ,
		'21.0' => '93.81 ' ,
		'21.5' => '95.45 ' ,
		'22.0' => '97.09 ' ,
		'22.5' => '98.74 ' ,
		'23.0' => '100.38 ' ,
		'23.5' => '102.03 ' ,
		'24.0' => '103.67 ' ,
		'24.5' => '105.32 ' ,
		'25.0' => '106.96 ' ,
		'25.5' => '108.61 ' ,
		'26.0' => '110.25 ' ,
		'26.5' => '111.90 ' ,
		'27.0' => '113.54 ' ,
		'27.5' => '115.18 ' ,
		'28.0' => '116.83 ' ,
		'28.5' => '118.47 ' ,
		'29.0' => '120.12 ' ,
		'29.5' => '121.76 ' ,
		'30.0' => '123.41 ' ,
		'31.0' => '126.58 ' ,
		'32.0' => '129.75 ' ,
		'33.0' => '132.93 ' ,
		'34.0' => '136.10 ' ,
		'35.0' => '139.28 ' ,
		'36.0' => '142.45 ' ,
		'37.0' => '145.62 ' ,
		'38.0' => '148.80 ' ,
		'39.0' => '151.97 ' ,
		'40.0' => '155.15 ' ,
		'41.0' => '158.32 ' ,
		'42.0' => '161.49 ' ,
		'43.0' => '164.67 ' ,
		'44.0' => '167.84 ' ,
		'45.0' => '171.02 ' ,
		'46.0' => '174.19 ' ,
		'47.0' => '177.36 ' ,
		'48.0' => '180.54 ' ,
		'49.0' => '183.71 ' ,
		'50.0' => '186.89 ' ,
		'51.0' => '190.06 ' ,
		'52.0' => '193.23 ' ,
		'53.0' => '196.41 ' ,
		'54.0' => '199.58 ' ,
		'55.0' => '202.76 ' ,
		'56.0' => '205.93 ' ,
		'57.0' => '209.10 ' ,
		'58.0' => '212.28 ' ,
		'59.0' => '215.45 ' ,
		'60.0' => '218.63 ' ,
		'61.0' => '221.80 ' ,
		'62.0' => '224.97 ' ,
		'63.0' => '228.15 ' ,
		'64.0' => '231.32 ' ,
		'65.0' => '234.50 ' ,
		'66.0' => '237.67 ' ,
		'67.0' => '240.84 ' ,
		'68.0' => '244.02 ' ,
		'69.0' => '247.19 ' ,
		'70.0' => '250.37 ');
		foreach ($weight_rate_arr_zone3 as $weight_s => $rate_s){
			if($weight_s == $weight){
				return $rate_s;
			}
			
		}
		
	}elseif($zone == 4){
		$weight_rate_arr_zone4 = array(
		'0.5' => '22.67' ,
		'1.0' => '22.67' ,
		'1.5' => '22.67' ,
		'2.0' => '22.67' ,
		'2.5' => '31.15' ,
		'3.0' => '31.15' ,
		'3.5' => '31.15' ,
		'4.0' => '31.15' ,
		'4.5' => '31.15' ,
		'5.0' => '31.15' ,
		'5.5' => '38.65' ,
		'6.0' => '40.49' ,
		'6.5' => '42.33' ,
		'7.0' => '44.17' ,
		'7.5' => '54.38' ,
		'8.0' => '56.56' ,
		'8.5' => '58.73' ,
		'9.0' => '60.90' ,
		'9.5' => '63.08' ,
		'10.0' => '65.25' ,
		'10.5' => '67.15' ,
		'11.0' => '69.05' ,
		'11.5' => '70.94' ,
		'12.0' => '72.84' ,
		'12.5' => '74.74' ,
		'13.0' => '76.64' ,
		'13.5' => '78.53' ,
		'14.0' => '80.43' ,
		'14.5' => '82.33' ,
		'15.0' => '84.23' ,
		'15.5' => '86.12' ,
		'16.0' => '88.02' ,
		'16.5' => '89.92' ,
		'17.0' => '91.82' ,
		'17.5' => '93.71' ,
		'18.0' => '95.61' ,
		'18.5' => '97.51' ,
		'19.0' => '99.41' ,
		'19.5' => '101.30' ,
		'20.0' => '103.20' ,
		'20.5' => '105.10' ,
		'21.0' => '107.00' ,
		'21.5' => '108.89' ,
		'22.0' => '110.79' ,
		'22.5' => '112.69' ,
		'23.0' => '114.59' ,
		'23.5' => '116.48' ,
		'24.0' => '118.38' ,
		'24.5' => '120.28' ,
		'25.0' => '122.18' ,
		'25.5' => '124.07' ,
		'26.0' => '125.97' ,
		'26.5' => '127.87' ,
		'27.0' => '129.77' ,
		'27.5' => '131.66' ,
		'28.0' => '133.56' ,
		'28.5' => '135.46' ,
		'29.0' => '137.36' ,
		'29.5' => '139.25' ,
		'30.0' => '141.15' ,
		'31.0' => '144.83' ,
		'32.0' => '148.51' ,
		'33.0' => '152.19' ,
		'34.0' => '155.87' ,
		'35.0' => '159.55' ,
		'36.0' => '163.23' ,
		'37.0' => '166.91' ,
		'38.0' => '170.59' ,
		'39.0' => '174.27' ,
		'40.0' => '177.95' ,
		'41.0' => '181.63' ,
		'42.0' => '185.31' ,
		'43.0' => '188.99' ,
		'44.0' => '192.67' ,
		'45.0' => '196.35' ,
		'46.0' => '200.03' ,
		'47.0' => '203.71' ,
		'48.0' => '207.39' ,
		'49.0' => '211.07' ,
		'50.0' => '214.75' ,
		'51.0' => '218.43' ,
		'52.0' => '222.11' ,
		'53.0' => '225.79' ,
		'54.0' => '229.47' ,
		'55.0' => '233.15' ,
		'56.0' => '236.83' ,
		'57.0' => '240.51' ,
		'58.0' => '244.19' ,
		'59.0' => '247.87' ,
		'60.0' => '251.55' ,
		'61.0' => '255.23' ,
		'62.0' => '258.91' ,
		'63.0' => '262.59' ,
		'64.0' => '266.27' ,
		'65.0' => '269.95' ,
		'66.0' => '273.63' ,
		'67.0' => '277.31' ,
		'68.0' => '280.99' ,
		'69.0' => '284.67' ,
		'70.0' => '288.35');
		foreach ($weight_rate_arr_zone4 as $weight_s => $rate_s){
			if($weight_s == $weight){
				return $rate_s;
			}
		}
	}elseif($zone == 5){
		$weight_rate_arr_zone5 = array(
		'0.5' => '27.34' ,
		'1.0' => '27.34' ,
		'1.5' => '27.34' ,
		'2.0' => '27.34' ,
		'2.5' => '38.56' ,
		'3.0' => '38.56' ,
		'3.5' => '38.56' ,
		'4.0' => '38.56' ,
		'4.5' => '38.56' ,
		'5.0' => '38.56' ,
		'5.5' => '48.22' ,
		'6.0' => '50.35' ,
		'6.5' => '52.47' ,
		'7.0' => '54.60' ,
		'7.5' => '67.02' ,
		'8.0' => '69.53' ,
		'8.5' => '72.04' ,
		'9.0' => '74.54' ,
		'9.5' => '77.05' ,
		'10.0' => '79.56' ,
		'10.5' => '82.00' ,
		'11.0' => '84.43' ,
		'11.5' => '86.87' ,
		'12.0' => '89.31' ,
		'12.5' => '91.75' ,
		'13.0' => '94.19' ,
		'13.5' => '96.62' ,
		'14.0' => '99.06' ,
		'14.5' => '101.50' ,
		'15.0' => '103.94' ,
		'15.5' => '106.38' ,
		'16.0' => '108.81' ,
		'16.5' => '111.25' ,
		'17.0' => '113.69' ,
		'17.5' => '116.13' ,
		'18.0' => '118.57' ,
		'18.5' => '121.00' ,
		'19.0' => '123.44' ,
		'19.5' => '125.88' ,
		'20.0' => '128.32' ,
		'20.5' => '130.76' ,
		'21.0' => '133.19' ,
		'21.5' => '135.63' ,
		'22.0' => '138.07' ,
		'22.5' => '140.51' ,
		'23.0' => '142.95' ,
		'23.5' => '145.38' ,
		'24.0' => '147.82' ,
		'24.5' => '150.26' ,
		'25.0' => '152.70' ,
		'25.5' => '155.14' ,
		'26.0' => '157.57' ,
		'26.5' => '160.01' ,
		'27.0' => '162.45' ,
		'27.5' => '164.89' ,
		'28.0' => '167.33' ,
		'28.5' => '169.76' ,
		'29.0' => '172.20' ,
		'29.5' => '174.64' ,
		'30.0' => '177.08' ,
		'31.0' => '181.54' ,
		'32.0' => '186.00' ,
		'33.0' => '190.46' ,
		'34.0' => '194.93' ,
		'35.0' => '199.39' ,
		'36.0' => '203.85' ,
		'37.0' => '208.31' ,
		'38.0' => '212.77' ,
		'39.0' => '217.24' ,
		'40.0' => '221.70' ,
		'41.0' => '226.16' ,
		'42.0' => '230.62' ,
		'43.0' => '235.08' ,
		'44.0' => '239.55' ,
		'45.0' => '244.01' ,
		'46.0' => '248.47' ,
		'47.0' => '252.93' ,
		'48.0' => '257.39' ,
		'49.0' => '261.86' ,
		'50.0' => '266.32' ,
		'51.0' => '270.78' ,
		'52.0' => '275.24' ,
		'53.0' => '279.70' ,
		'54.0' => '284.17' ,
		'55.0' => '288.63' ,
		'56.0' => '293.09' ,
		'57.0' => '297.55' ,
		'58.0' => '302.01' ,
		'59.0' => '306.48' ,
		'60.0' => '310.94' ,
		'61.0' => '315.40' ,
		'62.0' => '319.86' ,
		'63.0' => '324.32' ,
		'64.0' => '328.79' ,
		'65.0' => '333.25' ,
		'66.0' => '337.71' ,
		'67.0' => '342.17' ,
		'68.0' => '346.63' ,
		'69.0' => '351.10' ,
		'70.0' => '355.56' );	
		foreach ($weight_rate_arr_zone5 as $weight_s => $rate_s){
			if($weight_s == $weight){
				return $rate_s;
			}
				
		}	
	}
}

function wp_dog_breed_form($atts) {

	if(isset($_POST['submit_diet'])){
		$dog_name_1 = $_POST['dog_name_1'];
		$country_name_1  = $_POST['country_name_1'];
		$gender= $_POST['gender'];
		$neutered = $_POST['neutered'];
		$years = $_POST['years'];
		$months = $_POST['months']; 
		$age = $months.' months'.$years.' years';
		$dogbreeds = $_POST['dogbreeds']; 
		//$dog_food_type= $_POST['dog_food_type'];
		$dog_weight= $_POST['dog_weight'];
		$dog_type = $_POST['dog_type'];
		$how_active= $_POST['how_active'];
		$user_name= $_POST['user_name'];
		$user_email = $_POST['user_email'];
		$dog_food_type_arr = $_POST['dog_food_type'];
		$dog_food_tye= implode(', ', $dog_food_type_arr);

	if($how_active == 'lazy' && $dog_type == 'skinny'){
		$calorie_per_kg = 38;
	}
	elseif($how_active == 'active' && $dog_type == 'skinny'){
		$calorie_per_kg = 42;
	}elseif($how_active == 'hyper' && $dog_type == 'skinny'){
		$calorie_per_kg = 42;
	}elseif($how_active == 'lazy' && $dog_type == 'just_right'){
		$calorie_per_kg = 33;
	}
	elseif($how_active == 'active' && $dog_type == 'just_right'){
		$calorie_per_kg = 38;
	}elseif($how_active == 'hyper' && $dog_type == 'just_right'){
		$calorie_per_kg = 42;
	}elseif($how_active == 'lazy' && $dog_type == 'cubby'){
		$calorie_per_kg = 33;
	}
	elseif($how_active == 'active' && $dog_type == 'cubby'){
		$calorie_per_kg = 33;
	}elseif($how_active == 'hyper' && $dog_type == 'cubby'){
		$calorie_per_kg = 33;
	}
	
	$calorie_per_day = $calorie_per_kg * $dog_weight;
    $calorie_per_month = $calorie_per_day * 28;
    $calorie_per_month_k = $calorie_per_month / 1000;
    $food_calorie_per_kg = get_option('kcal_per_food');
        //$total_g = $calorie_per_month  / $food_calorie_per_kg ;
    $food_wt_kg = $calorie_per_month_k  / $food_calorie_per_kg ;
    //$total_kg = ceiling($total_kg, 1, PHP_ROUND_HALF_UP) ;
    $food_wt_kg = round($food_wt_kg, 2, PHP_ROUND_HALF_UP) ;
    

	//$total_kg = $total_g / 1000;

	//$country_name = get_page_by_title( 'Australia (AU)', , 'countries' );
	$country_obj = get_page_by_title($country_name_1, OBJECT, 'countries');
	$country_id = $country_obj->ID;
	$tax_str = get_field( "tax",$country_id);
	$tax = trim($tax_str,"%");
	

	$dog_food_price_per_kg_str = get_option('price_per_kg');
	$dog_food_price_per_kg = floatval($dog_food_price_per_kg_str);
	$threepl = get_option('threepl');
	$packaging = get_option('packaging');
	$total_kg = $food_wt_kg + 1;
	
	$kg_arr = explode('.', $total_kg);
	
	if($kg_arr[1][0] == 5 || $kg_arr[1][0] == 1 || $kg_arr[1][0] == 2 || $kg_arr[1][0] == 3 || $kg_arr[1][0] == 4){
		$total_kg_rr = $kg_arr[0].".5";
	}
	else if($kg_arr[1][0] == 6 || $kg_arr[1][0] == 7 || $kg_arr[1][0] == 8 || $kg_arr[1][0] == 9 ){
		$total_kg_rr = ceil($total_kg);
	}
	else{
		$total_kg_rr = round($total_kg);
	}
	//shipping
	$shipping_price = get_shipping_rate_by_country_weight($country_name_1,$total_kg_rr);
		

	$food_price_only = floatval($food_wt_kg) * $dog_food_price_per_kg;
	//$food_price_with_out_tax = $threepl + $packaging + $food_price_only + 23 * 1.15;
	$food_price_with_out_tax = $shipping_price + $threepl + $packaging + $food_price_only;
	$food_price_percent = $food_price_with_out_tax/100;
	$food_price_tax = $food_price_percent * $tax;
	$food_price_to_show = $food_price_with_out_tax + $food_price_tax;
	update_option('food_price_to_show',$food_price_to_show);
	$customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => 'shop_order', // WC orders post type
        'post_status' => 'wc-completed' // Only orders with status "completed"
    ) );
    // Count number of orders
    $count = count( $customer_orders );

    // return "true" when customer has already one order
    if ( $count > 0 ){
		$food_price_only = floatval($food_wt_kg) * $dog_food_price_per_kg;
		//$food_price_with_out_tax = $threepl + $packaging + $food_price_only + 23 * 1.15;
		$food_price_with_out_tax = $shipping_price + $threepl + $packaging + $food_price_only;
		$food_price_percent = $food_price_with_out_tax/100;
		$food_price_tax = $food_price_percent * $tax;
		$food_price = $food_price_with_out_tax + $food_price_tax;
		update_option('first_month','no');
	}else{
		$total_kg = $food_wt_kg * 0.625;
		$total_kg = $total_kg + 1;

		$kg_arr = explode('.', $total_kg);
		if($kg_arr[1][0] == 5 || $kg_arr[1][0] == 1 || $kg_arr[1][0] == 2 || $kg_arr[1][0] == 3 || $kg_arr[1][0] == 4){
			$total_kg_rr = $kg_arr[0].".5";
		}
		else if($kg_arr[1][0] == 6 || $kg_arr[1][0] == 7 || $kg_arr[1][0] == 8 || $kg_arr[1][0] == 9 ){
			$total_kg_rr = ceil($total_kg);
		}
		else{
			$total_kg_rr = round($total_kg);
		}
		$shipping_price_first = get_shipping_rate_by_country_weight($country_name_1,$total_kg_rr);

		update_option('first_month','yes');

		//
		$food_price_dis = 0.625;

		$food_wt_kg_first =  $food_price_dis * $food_price_only;

		$food_price_with_out_tax = $shipping_price_first +  $threepl + $packaging + $food_wt_kg_first;
		$food_price_only = round($food_price_only,2);

		$food_price_percent = $food_price_with_out_tax/100;
		$food_price_tax = $food_price_percent * $tax;
		$food_price = $food_price_with_out_tax + $food_price_tax;

		$food_price = round($food_price,2);
		$food_price_first = update_option('food_price_first',$food_price);
		$cart_page_disc = get_option('cart_page_disc');
		$cart_page_disc = floatval($cart_page_disc);
		$food_price_only = floatval($food_wt_kg) * $dog_food_price_per_kg;
		$food_price_dis_admin = $cart_page_disc/100;
		$food_price_discounted = $food_price * $food_price_dis_admin;
		$food_price = $food_price - $food_price_discounted;
	//$food_price = $food_price - $food_price_dis;
	}
    $dog_name_set1 = "Tailored organic beef recipe for ".$dog_name_1;
	$new_post = array(
			//'post_title' => $dog_name_1,			'post_title' => 'sdsdsd',
			'post_status' => 'publish',
			'post_type' => 'product'
			);
			$skuu = "custom-breed";
			$new_post_id = wp_insert_post($new_post);
			update_post_meta($new_post_id, '_sku', $skuu );
			update_post_meta( $new_post_id, '_price', $food_price );
			update_post_meta( $new_post_id,'_regular_price', $food_price );
			update_post_meta( $new_post_id,'_weight', $total_kg );
			update_post_meta( $new_post_id,'country_name', $country_name_1 );
			update_post_meta( $new_post_id,'dog_weight', $dog_weight );
			update_post_meta( $new_post_id,'how_active', $how_active );
			update_post_meta( $new_post_id,'dog_type', $dog_type );
			update_post_meta( $new_post_id,'user_name', $user_name );
			update_post_meta( $new_post_id,'user_email', $user_email );
			update_post_meta( $new_post_id,'dog_food_type', $dog_food_type );
			update_post_meta( $new_post_id,'neutered', $neutered );
			update_post_meta( $new_post_id,'dogbreeds', $dogbreeds );
			update_post_meta( $new_post_id,'gender', $gender );
			update_post_meta( $new_post_id,'age', $age );
			//global $woocommerce;
			//$woocommerce->cart->add_to_cart($new_post_id);
			//$cart_url = site_url().'/cart';				
			$cart_url = site_url().'/dog/shop/?add-to-cart='.$new_post_id;				
			?>
			<script type="text/javascript">
			 document.location.href="<?php echo $cart_url; ?>";
		   </script>
			<?php
	 
	}

ob_start();

	?>
				
    
  <link href="css/style.css" rel="stylesheet">

    
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  
 
<script>      
$(document).ready(function(){
    $("#add_dog").click(function(){
        $("#inner_sec").slideToggle("slow");
    });
});
    
</script>
    
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<?php 
		global $woocommerce;
    	$woocommerce->cart->empty_cart();
    ?>
    <div class="container" style="position:relative; margin-top: 50px;margin-bottom:30px;">
		<form method="post" id="formDogs">
            <div id="section1" class="col-md-12" style="">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:0%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>0%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           TELL US<br>ABOUT YOUR DOG
                        </h2>
                    </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-md-3">
                </div>    
                <div class="col-md-6 first_step">
                    
                        <div class="form-group" style="margin-bottom: 30px;">
                            <input type="text" class="form-control dog_name_1" placeholder="My dog’s name is..." required="required" name="dog_name_1">
                        </div>
                        <div class="form-group" style="margin-bottom: 30px;">
                        <select class="form-control" id="country_name" type="text" name="country_name_1" required="required">
                            <option>Country where your pup lives in...</option>
                            <?php
                            $args = array(
                                'post_type'        => 'countries',
								'orderby'        => 'name',
								'order'          => 'ASC',
                                'posts_per_page'        => -1
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) {
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    echo '<option value="'.get_the_title().'" shipping="'. get_post_meta(get_the_ID(), 'Shipping', true ).'" tax="'.get_post_meta(get_the_ID(), 'tax', true ).'">' . get_the_title() . '</option>';

                                } // end while
                            } // end if
                            wp_reset_query();
                            ?>
                        </select>
                        </div>
                        <!--<ul style="list-style: none; margin: 50px 0px 0px 0px; padding: 0px;" class="text-center">-->
                        <!--    <li class="plus_icon" id="add_dog"><span class="plus_icon_img"><img src="img/icon_1.png"></span>ADD ANOTHER DOG</li>-->
                        <!--</ul>-->
                        <!--<div class="inner_section" style="display: none;margin: 50px 0px;" id="inner_sec">-->
                        <!--    <div class="form-group" style="margin-bottom: 30px;">-->
                        <!--    <input type="text" class="form-control" placeholder="My dog’s name is..." name="dog_name_2">-->
                        <!--</div>-->
                        <!--<div class="form-group" style="margin-bottom: 30px;">-->
                        <!--<select class="form-control" type="text" name="country_name_2">-->
                        <!--      <option>Country where your pup lives in...</option>-->
                        <!--      <option value="Afghanistan">Afghanistan</option>-->
                        <!--      <option value="Albania">Albania</option>-->
                        <!--      <option value="Algeria">Algeria</option>-->
                        <!--      <option value="Andorra">Andorra</option>-->
                        <!--      <option value="Angola">Angola</option>-->
                        <!--</select>-->
                        <!--</div>-->
                        
                        <!--</div>-->
                    
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <button type="button" class="btn" id="go-sec-2">NEXT ❯</button>    
                    </div>
                </div>
            </div>
            </div>
            <div id="section2" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:14%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>14%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                            <span class="dogName"></span> IS A...
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-1" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6 step2">
                    <div class="row">
                        <div class="select_dog text-center">
                            <input id="boy" type="radio" name="gender"  value="boy" required="required" class="gender-cc boy"/>
                            <input id="girl" type="radio" name="gender" value="girl" required="required" class="gender-cc girl"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-3" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>
<!--                <div id="boy_section" style="display: none; position: relative;">-->
            <div id="section3" class="col-md-12" style="display: none;" >
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:29%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>29%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           IS <span class="bookie_name"></span>  <span class="is_book"></span> ?
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                 <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-2" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6 step3">
                    <div class="row">
                        <div class="select_dog text-center">
                         <input type="radio" name="neutered" value="Yes" required="required" class="neutered-cc neutered_yes"/>
                         <input type="radio" name="neutered" value="No" required="required" class="neutered-cc neutered_no"/>
                        </div>
                    </div>
                        <div class="section-title-header text-center">
                            <h2>
                                AND <span class="set_gender"></span> WAS BORN Before...
                            </h2>
                        </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Year</label>
                               <select name="years" width="100%" class="form-control">
                                <option value="1" width="100%">1 YEAR and ...</option>
                                <option value="2">2 YEARS and ...</option>
                                <option value="3">3 YEARS and ...</option>
                                <option value="4">4 YEARS and ...</option>
                                <option value="5">5 YEARS and ...</option>
                                <option value="6">6 YEARS and ...</option>
                                <option value="7">7 YEARS and ...</option>
                                <option value="8">8 YEARS and ...</option>
                                <option value="9">9 YEARS and ...</option>
                                <option value="10">10 YEARS and ...</option>
                                <option value="11">11 YEARS and ...</option>
                                <option value="12">12 YEARS and ...</option>
                                <option value="13">13 YEARS and ...</option>
                                <option value="14">14 YEARS and ...</option>
                                <option value="15">15 YEARS and ...</option>
                                <option value="16">16 YEARS and ...</option>
                                <option value="17">17 YEARS and ...</option>
                                <option value="18">18 YEARS and ...</option>
                                <option value="19">19 YEARS and ...</option>
                                <option value="20">20 YEARS and ...</option>
                                <option value="21">21 YEARS and ...</option>
                                <option value="22">22 YEARS and ...</option>
                                <option value="23">23 YEARS and ...</option>
                                <option value="24">24 YEARS and ...</option>
                                <option value="25">25 YEARS and ...</option>
                                <option value="26">26 YEARS and ...</option>
                                <option value="27">27 YEARS and ...</option>
                                <option value="28">28 YEARS and ...</option>
                                <option value="29">29 YEARS and ...</option>
                                <option value="30">30 YEARS and ...</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Month</label>
                        <select name="months" width="100%" class="form-control">
                            <option value="1" width="100%">1 MONTH.</option>
                            <option value="2">2 MONTHS.</option>
                            <option value="3">3 MONTHS.</option>
                            <option value="4">4 MONTHS.</option>
                            <option value="5">5 MONTHS.</option>
                            <option value="6">6 MONTHS.</option>
                            <option value="7">7 MONTHS.</option>
                            <option value="8">8 MONTHS.</option>
                            <option value="9">9 MONTHS.</option>
                            <option value="10">10 MONTHS.</option>
                            <option value="11">11 MONTHS.</option>
                        </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-4" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>
            <div id="section4" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:43%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>43%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                            <span class="bookie_new"></span> IS A...
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-3" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6 dog_breed_sel">
                        <div class="form-group" style="margin-bottom: 30px;">
                        <select id="dogbreeds" name="dogbreeds" class="form-control" type="text">
						   <option>Your dog’s breed...</option>
                            <?php
                            $args = array(
                                'post_type'        => 'dogs',
								'orderby'        => 'name',
								'order'          => 'ASC',
								'posts_per_page'        => -1
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) {
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    echo '<option value="'. get_the_title() . '">' . get_the_title() . '</option>';

                                } // end while
                            } // end if
                            wp_reset_query();
                            ?>
                        </select>
                        </div>
                        <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           AND <span class="eating_value"></span> IS CURRENTLY EATING...
                        </h2>
                        </div>
                        <div class="row">
                            <div class="select_dog text-center">
                                 <input type="checkbox" name="dog_food_type[]" value="dry food"  class="dog_food_type food-cc food_dry"/>
                                 <input type="checkbox" name="dog_food_type[]" value="wet food"  class="dog_food_type food-cc food_wet"/>
                                 <input type="checkbox" name="dog_food_type[]" value="home made food"  class="dog_food_type food-cc food_home"/>
                                 <input type="checkbox" name="dog_food_type[]" value="rawfood"  class="dog_food_type food-cc food_raw"/>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-5" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>
            <div id="section5" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:57%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>57%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                            <span class="weight_about"></span> WEIGHTS ABOUT
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-4" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 30px;">
                            <input type="number" id="dog_weight" name="dog_weight" class="form-control" placeholder="Type how much your dog weights in kilograms (KG)...">
                        </div>
                        <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           <span class="weight_gender"></span> IS...
                        </h2>
                        </div>
                        <div class="row">
                            <div class="select_dog text-center">
                                 <input type="radio" name="dog_type" value="skinny" required="required" class="kilograms-cc skinny"/>
                                 <input type="radio" name="dog_type" value="just_right" required="required" class="kilograms-cc justright"/>
                                 <input type="radio" name="dog_type" value="cubby" required="required" class="kilograms-cc cubby"/>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-6" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>
            <div id="section6" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:71%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>71%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                            HOW ACTIVE IS <span class="average"></span>,<br>ON AVERAGE?
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-5" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6">
                        <div class="row">
                            <div class="select_dog text-center">
                                 <input type="radio" name="how_active"  value="lazy" required="required" class="actives-cc lazy"/>
                                 <input type="radio" name="how_active" value="active" required="required" class="actives-cc active_img"/>
                                 <input type="radio" name="how_active" value="hyper" required="required" class="actives-cc hyper"/>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-7" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>

            <div id="section7" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:86%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>86%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           TELL US A BIT<br>ABOUT YOURSELF
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-6" value="❮ BACK">    
                    </div>
                </div>   
                <div class="col-md-6">
                    <form>
                        <div class="form-group" style="margin-bottom: 30px;">
                            <input type="text" name="user_name" class="form-control" placeholder="My name is..." required="required">
                        </div>
                        <div class="form-group" style="margin-bottom: 30px;">
                            <input type="email" name="user_email" class="form-control" placeholder="My email is.." required="required">
                        </div>
                        <div class="row">
                            <div class="col-md-2">
<!--                                <img src="img/button.png" width="100%">-->
                                <input type="checkbox" id="toggle-one" checked data-toggle="toggle" data-size="large" class="btn_togg">
                            </div>
                            <div class="col-md-10 section-title">
                                <p style="line-height: 1;">I’m happy to receive nutritional advice from Rocketo’s inhouse vet, funny dog pictures and occasional promotional offers.<br><span style="color: #76b2a8;"> See Privacy Policy.</span></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <button type="submit" name="submit_diet" class="btn">CALCULATE DIET</button>    

                    </div>
                </div>
            </div>
        </div>
</form>    
              
</div>
            
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>     
  <script>
  
$(function() {
    $('#toggle-one').bootstrapToggle();
  })       
var  dog_name_1;      
$(document).ready(function(){
  $("#go-sec-2").click(function(){
     dog_name_1 = $('.dog_name_1').val();
     var country_name11 = $('#country_name :selected').text();
     console.log(dog_name_1);
	if(dog_name_1 == "" || country_name11 == "Country where your pup lives in..." ){
		if(country_name11 == "Country where your pup lives in..." ){
			$( "#country_name" ).focus();
			//$('.dogName').html(dog_name_1);  
		 }
		if(dog_name_1 == "" ){
			$( ".dog_name_1" ).focus();
		}
	 }else{
		$("#section2").fadeToggle()
		&& $("#section1").fadeOut();
		$('.dogName').html(dog_name_1);  
	 }
  });
    
  $("#go-sec-3").click(function(){
     var boy =  $(".boy").prop("checked");
     var girl = $(".girl").prop("checked");
    if(boy == true){
       $('.set_gender').html(dog_name_1); 
       $('.weight_gender').html(dog_name_1); 
       $('.eating_value').html(dog_name_1);
       $('.bookie_name').html(dog_name_1); 
        $('.bookie_new').html(dog_name_1); 
        $('.weight_about').html(dog_name_1);
        $('.average').html(dog_name_1);
       $('.is_book').html('NEUTERED');    
    }
      if(girl == true){
          $('.set_gender').html(dog_name_1);
          $('.weight_gender').html(dog_name_1);
          $('.eating_value').html(dog_name_1);
          $('.bookie_name').html(dog_name_1);
          $('.bookie_new').html(dog_name_1);
          $('.weight_about').html(dog_name_1);
          $('.average').html(dog_name_1);
          $('.is_book').html('SPAYED');
      }
    if(boy == true || girl == true){    	  	
    	$("#section3").fadeToggle() && $("#section2").fadeOut();
	}else{
		alert('Please select any gender.');
		$(".gender-cc").focus();
	}


  });
    
  $("#go-sec-4").click(function(){
	  var neutered_yes =  $(".neutered_yes").prop("checked");
	  var neutered_no =  $(".neutered_no").prop("checked");
	  console.log(neutered_yes);
	  console.log(neutered_no);
	   if(neutered_yes == true || neutered_no == true){    	  	
    	$("#section4").fadeToggle() && $("#section3").fadeOut();  
	}else{
		alert('Please select Yes or No.');
		$(".gender-cc").focus();
	}
/* 	if(neutered_yes == false || neutered_no == false){
		if(neutered_yes == false || neutered_no == false){
			//$('.dogName').html(dog_name_1);  
		 }
	 }else{
		$("#section4").fadeToggle() && $("#section3").fadeOut();  
	 } */
  });
    
  $("#go-sec-5").click(function(){
     var dog_breed_sel = $('#dogbreeds :selected').text();
    // var dog_food_type = $(".dog_food_type").prop("checked");
		var food_dry =  $(".food_dry").prop("checked");
		var food_wet =  $(".food_wet").prop("checked");
		var food_home =  $(".food_home").prop("checked");
		var food_raw =  $(".food_raw").prop("checked");
     
   if((food_dry == true || food_wet == true || food_home == true || food_raw == true) && dog_breed_sel != "Your dog’s breed..."){    	  	
	  console.log(food_dry);
	  $("#section5").fadeToggle() && $("#section4").fadeOut();
/* 		if(){
			$("#dogbreeds").focus();
		}
		 if(food_dry == false || food_wet == false || food_home == false || food_raw == false){    	  	
			  alert('Please select the food type.');
	   } */
   }else{	
	   alert('Please select the food type and dog breed.');
   }
  });
    
  $("#go-sec-6").click(function(){
    var dog_weight1 = $('#dog_weight').val();
	var skinny =  $(".skinny").prop("checked");
	var cubby =  $(".cubby").prop("checked");
	var justright =  $(".justright").prop("checked");
    if ((justright == true || cubby == true || skinny == true)&& dog_weight1) {
    	$("#section6").fadeToggle()	&& $("#section5").fadeOut();
	}
	else{
		$("#dog_weight").focus();
		alert('Please select the food weight and skin type.');
	}


  });
    
  $("#go-sec-7").click(function(){
	var lazy =  $(".lazy").prop("checked");
	var active_img =  $(".active_img").prop("checked");
	var hyper =  $(".hyper").prop("checked");
	if (hyper == true || active_img == true || lazy == true) {
    	    $("#section7").fadeToggle() && $("#section6").fadeOut();
	}
	else{
		alert('Please select the one option.');
	}

  });
    
  $("#go-sec-8").click(function(){
    
  	var user_name1 = $('#user_name').val();

    if (user_name1) {
    	$("#section8").fadeToggle() && $("#section7").fadeOut();
	// $('#formDogs').submit();   
	}else{
		$("#user_name").focus();
	}
  });
    
  $("#bk-sec-1").click(function(){
    $("#section1").fadeToggle()
 	&& $("#section2").fadeOut();
  });
  
  $("#bk-sec-2").click(function(){
    $("#section2").fadeToggle()
 	&& $("#section3").fadeOut();
  });
    
  $("#bk-sec-3").click(function(){
    $("#section3").fadeToggle()
 	&& $("#section4").fadeOut();
  });
    
  $("#bk-sec-4").click(function(){
    $("#section4").fadeToggle()
 	&& $("#section5").fadeOut();
  });
    
  $("#bk-sec-5").click(function(){
    $("#section5").fadeToggle()
 	&& $("#section6").fadeOut();
  });
    
  $("#bk-sec-6").click(function(){
    $("#section6").fadeToggle()
 	&& $("#section7").fadeOut();
  });
    
  $("#bk-sec-7").click(function(){
    $("#section7").fadeToggle() && $("#section8").fadeOut();
  });
    
    $("#go-sec-G4").click(function(){
    $("#sectionG4").fadeToggle()
 	&& $("#sectionG3").fadeOut();
  });
    
    $("#go-sec-G5").click(function(){
    $("#sectionG5").fadeToggle()
 	&& $("#sectionG4").fadeOut();
  });
    
     $("#go-sec-G6").click(function(){
    $("#sectionG6").fadeToggle()
 	&& $("#sectionG5").fadeOut();
  });
    
     $("#go-sec-G7").click(function(){
    $("#section7").fadeToggle()
 	&& $("#sectionG6").fadeOut();
  });
    
    $("#bk-sec-G2").click(function(){
    $("#section2").fadeToggle()
 	&& $("#sectionG3").fadeOut();
  });
    
    $("#bk-sec-G3").click(function(){
    $("#sectionG3").fadeToggle()
 	&& $("#sectionG4").fadeOut();
  });
    
    $("#bk-sec-G4").click(function(){
    $("#sectionG4").fadeToggle()
 	&& $("#sectionG5").fadeOut();
  });
    
    $("#bk-sec-G5").click(function(){
    $("#sectionG5").fadeToggle()
 	&& $("#sectionG6").fadeOut();
  });
    
    $("#bk-sec-6").click(function(){
    $("#sectionG6").fadeToggle()
 	&& $("#section7").fadeOut();
  });
    
 $(".boy").click(function(){
      $( ".boy" ).attr( "checked", true );
     $( ".girl" ).attr( "checked", false );
 })
 
 $(".girl").click(function(){
      $( ".boy" ).attr( "checked", false );
      $( ".girl" ).attr( "checked", true );
    
 })
});
    
  </script>
</form>
	<?php
$result = ob_get_clean();
return $result;
	}
function dog_breed_script_back_css() {

}

function dog_breed_script_front_css() {
		/* CSS */
        wp_register_style('dog_breed_style', plugins_url('css/dog_breed.css',__FILE__));
        wp_enqueue_style('dog_breed_style',plugins_url('css/dog_breed.css',__FILE__),10);
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function dog_breed_script_back_js() {
	
}



function dog_breed_script_front_js() {
 	
			   
        wp_register_script('dog_breed_script', plugins_url('js/dog_breed.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('dog_breed_script');

}
