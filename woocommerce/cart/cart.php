<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<div class="cart_left_main">

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>



	<div class="section-title-panel cart_right_title">
	    <h2>
	        START ONLY FROM<br><div id="total_price_show"></div> € PER DAY
	    </h2>
	</div>
	<div class="section-title cart_right_desc">
		<?php $cart_page_disc_show = get_option('cart_page_disc'); ?>
		<?php $food_price_first = get_option('food_price_first'); ?>

	    <?php if($cart_page_disc_show == '0'){ ?>
	    	<p style="line-height: 1;">Your first 4 weeks starter pack will be <span><?php echo $food_price_first ;?></span> € with <span><?php echo $cart_page_disc_show ;?></span>% discount and it will cost you only <span id="50_per_total"></span> €.</p>
	    	<p style="line-height: 1;">After that, your 4 weeks food will cost <span id="price_inc_delivery"></span> € including delivery. </p>
	    <?php }  else{ ?>
	   		<p style="line-height: 1;">Your first month starter pack will be <span><?php echo $food_price_first ;?></span> € with <span><?php echo $cart_page_disc_show ;?></span>% discount and it will cost you only <span id="50_per_total"></span> €.</p>
	    	<p style="line-height: 1;">After that, your monthly food will cost <span id="price_inc_delivery"></span> € including delivery. </p>	    	
		<?php } ?>

		<p style="line-height: 1;">Remember that there is no commitment. You are free to stop your deliveries anytime. </p>
	</div>
	<hr class="bottom-line">
	<div class="section-title-panel">
		<h2>ORDER</h2>
	</div>
	
	<div class="mk-grid">
	<div class="mk-col-2-1">
	    <div class="section-title">
	        <p style="line-height: 1;">28 days supply of organic dog food. Always FREE delivery.</p>
	    </div>
	</div>
	
	</div>	


	<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
		<thead>
			<tr>
				<th class="product-remove">&nbsp;</th>
				<th class="product-thumbnail">&nbsp;</th>
				<th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="product-price"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
				<th class="product-quantity"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
				<th class="product-subtotal"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						<td class="product-remove">
							<?php
								// @codingStandardsIgnoreLine
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
							?>
						</td>

						<td class="product-thumbnail">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

						if ( ! $product_permalink ) {
							echo wp_kses_post( $thumbnail );
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), wp_kses_post( $thumbnail ) );
						}
						?>
						</td>

						<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
						<?php
						if ( ! $product_permalink ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
						} else {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
						}

						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>' ) );
						}
						?>
						</td>

						<td class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</td>

						<td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
						<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input( array(
								'input_name'   => "cart[{$cart_item_key}][qty]",
								'input_value'  => $cart_item['quantity'],
								'max_value'    => $_product->get_max_purchase_quantity(),
								'min_value'    => '0',
								'product_name' => $_product->get_name(),
							), $_product, false );
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						?>
						</td>

						<td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</td>
					</tr>
					<?php
				}
			}
			?>

			<?php do_action( 'woocommerce_cart_contents' ); ?>

			<tr>
				<td colspan="6" class="actions">

					<?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon">
							<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
					<?php } ?>

					<button type="submit" class="button update_cart" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
				</td>
			</tr>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</tbody>
	</table>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div class="cart-collaterals">
	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );
	?>
</div>

<?php $cart_page_disc_show = get_option('cart_page_disc'); ?>
<?php $first_month_check = get_option('first_month'); ?>
<?php $food_price_to_show = get_option('food_price_to_show'); ?>
<?php $food_price_first = get_option('food_price_first'); ?>
<input type="hidden" id="discount_price_dynamic" name="discount_price_dynamic" value="<?php echo $cart_page_disc_show; ?>">
<input type="hidden" id="food_price_to_show" name="food_price_to_show" value="<?php echo $food_price_to_show; ?>">
<input type="hidden" id="first_month" name="first_month" value="<?php echo $first_month_check; ?>">
<input type="hidden" id="food_price_first" name="food_price_first" value="<?php echo $first_month_check; ?>">

<script type="text/javascript">
	jQuery(document).ready(function(){
		var first_month_check = jQuery("#first_month").val();
		var food_price_to_show = jQuery("#food_price_to_show").val();
		console.log(first_month_check);
		console.log(food_price_to_show);
		food_price_to_show = parseFloat(food_price_to_show);
		var total_price_final = jQuery(".cart-subtotal .woocommerce-Price-amount.amount").text();
		var shipping_price1 = jQuery(".shipping .woocommerce-Price-amount.amount").text();
		var shipping_price = shipping_price1.substr(1);
		
		var total_price_final_trimmed = total_price_final.substr(1);

		//Calculating discount price
		var percentage_tocalc = jQuery("#discount_price_dynamic").val();
		console.log("percentage_tocalc.."+percentage_tocalc);
		
		//Percentage
		if(first_month_check == 'no'){
		var discount_per = total_price_final_trimmed * percentage_tocalc/100;
		var total_price_50_per = total_price_final_trimmed - discount_per;
		}else{
			
			var total_price_50_per = total_price_final_trimmed;
			//var food_price_to_show = food_price_to_show;
		}
		
		//Price per day
		var total_price_perday = parseFloat(total_price_final_trimmed) / 28;
		
		console.log("total_price_50_per.."+total_price_50_per);

		var total_price_plus_delivery = total_price_50_per;
		
		
		jQuery( "#50_per_total" ).html(parseFloat(total_price_50_per).toFixed(2));
		jQuery( "#price_inc_delivery" ).html(food_price_to_show.toFixed(2));
		//jQuery( ".cart-subtotal .woocommerce-Price-amount" ).clone().prependTo( "#total_price_show" );
		jQuery( "#total_price_show" ).html(total_price_perday.toFixed(2));

	});	
</script>
</div>
<?php do_action( 'woocommerce_after_cart' ); ?>
